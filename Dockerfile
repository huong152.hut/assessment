FROM node:16-alpine

WORKDIR /www

# install nodejs package
ADD package.json yarn.lock /www/
RUN yarn && yarn cache clean

ADD . /www

# build && clean dev dependencies
RUN yarn tsc && yarn install --production

CMD [ "node", "dist/index.js" ]
