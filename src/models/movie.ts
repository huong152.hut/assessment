import { Entity, Column, PrimaryGeneratedColumn, BaseEntity } from "typeorm"
import { Actor } from "../interfaces/movie"

@Entity()
export class Movie extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    title: string

    @Column()
    description: string

    @Column({ type: 'jsonb' })
    actor: Actor
}