import { describe, it, expect, beforeAll, afterAll } from '@jest/globals'
import supertest from "supertest"
import app from '../app'
import { ResponseStatus } from '../common/constant'
import dataSource from '../dataSource'

beforeAll(async () => {
    await dataSource.initialize()
})

afterAll(async () => {
    await dataSource.destroy()
})

describe("POST /api/movie", () => {
    it("invalid key", (done) => {
        supertest(app)
            .post('/api/movie')
            .set('api_key', 'apikey')
            .expect(400, done)
    })

    it("wrong input", (done) => {
        supertest(app)
            .post('/api/movie')
            .set('api_key', 'api_key')
            .send({ actor: { name: 'name', age: 10 } })
            .expect(200)
            .then(res => {
                expect(res.body.status).toEqual(ResponseStatus.Error)
                done()
            })
            .catch(e => {
                console.log(e.toString())
                done(e)
            })
    })

    it("valid key", (done) => {
        supertest(app)
            .post('/api/movie')
            .set('api_key', 'api_key')
            .send({ actor: { name: 'name', age: 10 }, title: "title", description: "description" })
            .expect(200)
            .then(res => {
                expect(res.body.status).toEqual(ResponseStatus.Success)
                done()
            })
            .catch(e => {
                console.log(e.toString())
                done(e)
            })
    })
})