import app from './app'
import dataSource from './dataSource'

// run app
const port = process.env.PORT || 8080
dataSource.initialize().then(
    () => {
        app.listen(port, () => {
            console.log(`Server is running at port ${port}`);
        });
    }
)