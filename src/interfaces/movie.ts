export interface Actor {
    name: string
    age: number
}
export interface MovieDto {
    title: string
    description: string
    actor: Actor
}