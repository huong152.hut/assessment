import { Request, Response } from 'express'
import { ResponseStatus } from '../common/constant'
import { createMovie } from '../services/movie'

export async function create(req: Request, res: Response) {
    let error = await createMovie(req.body)
    res.send({
        status: error ? ResponseStatus.Error : ResponseStatus.Success,
        msg: error || null
    })
}