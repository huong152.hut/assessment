import express, { Express } from 'express';
import { apiAuth } from './middlewares/apiAuth'

// express
const app: Express = express();
app.use(express.json())

// routers
const routes = ['movie']
for (let route of routes) {
    const router = require(`./routes/${route}`).default
    app.use('/api', apiAuth, router)
}

export default app

