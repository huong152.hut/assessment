import dotenv from 'dotenv'
import path from 'path'

const configFile = process.env.NODE_ENV ? `.env_${process.env.NODE_ENV}` : '.env'
dotenv.config({ path: path.join(process.cwd(), configFile) })