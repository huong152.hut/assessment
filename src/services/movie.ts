import { MovieDto } from "../interfaces/movie";
import { Movie } from "../models/movie"

export async function createMovie(dto: MovieDto): Promise<string | undefined> {
    try {
        const movie = new Movie()
        movie.actor = dto.actor
        movie.description = dto.description
        movie.title = dto.title
        await movie.save()
    } catch (e: any) {
        return e.toString()
    }
}