import { describe, it, expect, beforeAll, afterAll } from '@jest/globals'
import dataSource from "../dataSource"
import { Movie } from '../models/movie'
import { createMovie } from '../services/movie'

beforeAll(async () => {
    await dataSource.initialize()
})

afterAll(async () => {
    await dataSource.destroy()
})

describe("Movie service", () => {
    it("create", async () => {
        let beforeCreate = await Movie.count()
        await createMovie({ actor: { name: 'name', age: 10 }, title: "title", description: "description" })
        let afterCreate = await Movie.count()
        expect(afterCreate - beforeCreate).toBe(1)
    })
})