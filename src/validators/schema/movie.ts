import Joi from 'joi'

export const MovieSchema = Joi.object({
    title: Joi.string().min(1).max(256).required(),
    description: Joi.string().min(1).max(256).required(),
    actor: Joi.object({
        name: Joi.string().min(1).max(256).required(),
        age: Joi.number().integer().min(5).max(100).required()
    }).required()
})