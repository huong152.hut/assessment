import { Request, Response, NextFunction } from 'express'
import { ResponseStatus } from '../common/constant'
import { MovieSchema } from './schema/movie'

export const movieValidator = (req: Request, res: Response, next: NextFunction) => {
    try {
        const result = MovieSchema.validate(req.body)
        if (result.error) {
            res.send({ status: ResponseStatus.Error, msg: result.error.details.map(it => it.message).join("; ") })
        } else {
            next()
        }
    } catch (e: any) {
        res.send({ status: ResponseStatus.Error, msg: e.toString() })
    }
}