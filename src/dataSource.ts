import "reflect-metadata"
import path from 'path'
import './config'
import { DataSource } from 'typeorm'
import { Movie } from './models/movie'
console.log(__dirname)
const AppDataSource = new DataSource({
    type: "postgres",
    host: process.env.DB_HOST,
    port: process.env.DB_PORT as any,
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    entities: [Movie],
    migrations: [path.join(__dirname, '/migration/*{.ts,.js}')]
})

export default AppDataSource