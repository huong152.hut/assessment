import { Request, Response, NextFunction } from 'express'

export const apiAuth = (req: Request, res: Response, next: NextFunction) => {
    if (req.headers['api_key'] == process.env.API_KEY) {
        next()
    } else {
        res.status(400).end()
    }
}