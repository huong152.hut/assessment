import { Router } from 'express'
import { create } from '../controllers/Movie'
import { movieValidator } from '../validators'

const router = Router()

router.post('/movie', movieValidator, create)

export default router



